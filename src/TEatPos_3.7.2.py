"Return a list of supposed position of a TE."

import pysam
import argparse


# Shell interface

parser = argparse.ArgumentParser(
                    prog = 'TEatPos.py',
                    description = 'Associate find TE with a supposed position in the reference genome. Return a bed file.',
                    epilog = 'For more information, contact pierre.gerenton@etu.univ-lyon1.fr.')

parser.add_argument("reads2TE",
                    help = "Path of a SAM file with the alignment of the reads with a list of consensus TE. \n"
                           "WARNING : hard-clipping should be replace by hard-clipping in the alignment \n"
                           "WARNING : unaligned line should be delete")
parser.add_argument("reads2genome",
                    help = "Path of a SAM file with the alignment of the reads with the reference genome.")
parser.add_argument("-t","--tabular-format",
                    action="store_true",
                    help="Instead of writing a bed file, will write the output as a lighter tabular format with all TE,"
                    "even those without any position in the genome.")
parser.add_argument("-o","--output",
                    default=None,
                    help="Path of the output file (if not present, write in std:in)")

args = parser.parse_args()

# Class

class TE:
    """Reprensent a TE (or fragment) find in a read."""

    # Constructor

    def __init__(self, name, read_name, read_start, read_end):
        """Create the TE object.
        name (str) : the name of the TE
        read_name (str) : the name/id of the read
        read_start (int) : the start position of the TE on the read (inclusive, 5'3')
        read_end (int) : the end position of the TE on the read (inclusive, 5'3')
        chr_name (str) : the name of the chr/contig of the TE on the genome
        genome_start (int) : the supposed start position of the TE on the genome (inclusive, 5'3')
        genome_end (int) : the supposed end position of the TE on the genome (inclusive, 5'3')
        indicator (int) : integer showing the indication about the inference of position
        (more information in the documentation for get_indicator)"""
        self.name = name
        self.read_name = read_name
        self.read_start = read_start
        self.read_end = read_end
        self.chr_name = None
        self.genome_start = None
        self.genome_end = None
        self.indicator = 0

    # Getters

    def get_name(self):
        return self.name
    def get_read_name(self):
        return self.read_name

    def get_read_start(self):
        return self.read_start
    def get_read_end(self):
        return self.read_end
    def get_read_coord(self):
        return self.read_start, self.read_end
    
    def get_chr_name(self):  # return None if not defined
        return self.chr_name
    def get_genome_start(self):  # return None if not defined
        return self.genome_start
    def get_genome_end(self):  # return None if not defined
        return self.genome_end
    def get_genome_coord(self):  # return None if not defined
        return self.genome_start, self.genome_end
    
    def get_indicator(self):
        """Return an indicator based of the inference of the genomic position.
        0 : the genomic position is unaviable
        1 : the genomic position are include in the read2genome alignment
        2 : the end pos is included but the start wasn't
        3 : the start pos is included but the end wasn't
        4 : the start and end pos is in the soft-clipping part at the left of the cigarstring
        5 : the start and the end is in the soft-clipping part at the right of the cigarstring
        6 : the alignment is include in the TE
        """
        return self.indicator

    # Setters

    def set_chr_name(self, chr_name):
        """chr_name (str) : nom du chromosome/contig"""
        self.chr_name = chr_name
    def set_genome_start(self, genome_start):
        """genome_start (int) : position de début du TE sur le chromosome"""
        self.genome_start = genome_start
    def set_genome_end(self, genome_end):
        """genome_end (int) : position de fin du TE sur le chromosome"""
        self.genome_end = genome_end
    def set_genome_coord(self, genome_start, genome_end):
        """Set the genomic coordinates of the TE.
        genome_start (int) : position de début du TE sur le chromosome
        genome_end (int) : position de fin du TE sur le chromosome"""
        self.genome_start = genome_start
        self.genome_end = genome_end
    def set_indicator(self, ind):
        """ind (int) : indicator"""
        self.indicator = ind

    # Method

    def __str__(self):
        ind = self.get_indicator()
        if ind == 0:
            genome_coord = ""
        else:
            genome_coord = ";GN:" + self.chr_name + ";GS:" + str(self.genome_start) + ";GE:" + str(self.genome_end)
        string = "<TE " + self.name + ";RN:" + self.read_name + ";RS:" + str(self.read_start) + ";RE:" + str(self.read_end) + genome_coord + ";IN:" + str(self.indicator) + ">"
        return string
    
    def __repr__(self):
        return str(self)



# class Read:
#     """Represent a read with 1 or multiple TE."""

#     # Constructor

#     def __init__(self, id):
#         """Create the read object.
#         id (str) : the id/name of the read
#         elements (TE list) : list of transposable element found in that read"""
#         self.id = id
#         self.elements = []
#         self.current_index = 0 # for the iteration 

#     # Getters

#     def get_id(self):
#         return self.id

#     # Methods

#     def __iter__(self):
#         """Return an iterator (the object itself)"""
#         self.current_index = 0
#         return self
#     def __next__(self):
#         """Return the next value of the iterator."""
#         if self.current_index >= len(self.elements):
#             raise StopIteration
#         else:
#             current_position = self.current_index
#             self.current_index += 1
#             return self.elements[current_position]

#     def add_element(self, element):
#         """Add a new TE to the read."""
#         self.elements.append(element)


# Funcions

def updateTE(transposon, ali):
    """Update information of a TE to add the alignment on the genome.
    transposon (TE) : the TE to update
    ali (pysam.AlignedSegment) : the line of the alignment of the read against the genome."""
    # Syntax : 
    # name-of-alignment_coordonates-of-this-seq_start-end
    read2TE_read_start, read2TE_read_end = transposon.get_read_coord()
    read2genome_read_start = ali.query_alignment_start + 1
    read2genome_read_end = ali.query_alignment_end
    chr_name = ali.reference_name
    read2genome_genome_start = ali.reference_start +1
    read2genome_genome_end = ali.reference_end
    cigar = ali.cigartuples
    if cigar is None:
        return transposon
    if ali.is_reverse:
        read_length = ali.infer_query_length()
        read2TE_read_start, read2TE_read_end = read_length + 1 - read2TE_read_end, read_length + 1 - read2TE_read_start


    if read2TE_read_start < read2genome_read_start:
        TE2genome_TE_start = read2genome_genome_start - (read2genome_read_start-read2TE_read_start)
        if read2TE_read_end < read2genome_read_start:
            TE2genome_TE_end = read2genome_genome_start - (read2genome_read_start-read2TE_read_end)
            indicator = 4
        elif read2TE_read_end <= read2genome_read_end:
            pos_read = read2genome_read_start
            pos_genome = read2genome_genome_start
            objective_pos_read = read2TE_read_end
            for bloc in cigar:
                letter = bloc[0]
                if letter == 0 or letter == 7 or letter == 8:  # M, X or =
                    pos_read += bloc[1]  
                    pos_genome += bloc[1]                    
                elif letter == 1:  # I
                    pos_read = min(pos_read + bloc[1], objective_pos_read)
                elif letter == 2 or letter == 3:  # D or N
                    pos_genome += bloc[1]
                else:  # S, H, B or P
                    pass
                if pos_read >= objective_pos_read:  # when we go to far
                    TE2genome_TE_end = pos_genome - (pos_read - objective_pos_read)
                    break
            indicator = 2
        else :  # read2TE_read_end > read2genome_read_end
            TE2genome_TE_end = read2genome_genome_end + (read2TE_read_end - read2genome_read_end)
            indicator = 6
    elif read2TE_read_start <= read2genome_read_end:
        pos_read = read2genome_read_start
        pos_genome = read2genome_genome_start
        objective_pos_read = read2TE_read_start
        for bloc in cigar:
            letter = bloc[0]
            if letter == 0 or letter == 7 or letter == 8:  # M, X or =
                pos_read += bloc[1]  
                pos_genome += bloc[1]                    
            elif letter == 1:  # I
                pos_read = min(pos_read + bloc[1], objective_pos_read)
            elif letter == 2 or letter == 3:  # D or N
                pos_genome += bloc[1]
            else:  # S, H, B or P
                pass
            if pos_read >= objective_pos_read:  # when we go to far
                TE2genome_TE_start = pos_genome - (pos_read - objective_pos_read)
                break
        if read2TE_read_end <= read2genome_read_end:
            pos_read = read2genome_read_start
            pos_genome = read2genome_genome_start
            objective_pos_read = read2TE_read_end
            for bloc in cigar:
                letter == bloc[0] 
                if letter == 0 or letter == 7 or letter == 8:  # M, X or =
                    pos_read += bloc[1]  
                    pos_genome += bloc[1]                    
                elif letter == 1:  # I
                    pos_read = min(pos_read + bloc[1], objective_pos_read)
                elif letter == 2 or letter == 3:  # D or N
                    pos_genome += bloc[1]
                else:  # S, H, B or P
                    pass
                if pos_read >= objective_pos_read:  # when we go to far
                    TE2genome_TE_end = pos_genome - (pos_read - objective_pos_read)
                    break
            indicator = 1
        else:  # read2TE_read_end > read2genome_read_end
            TE2genome_TE_end = read2genome_genome_end + (read2TE_read_end - read2genome_read_end)
            indicator = 3
    else:  # read2TE_read_start > read2genome_read_end
        TE2genome_TE_start = read2genome_genome_end + (read2TE_read_start - read2genome_read_end)
        TE2genome_TE_end = read2genome_genome_end + (read2TE_read_end - read2genome_read_end)
        indicator = 5
    
    name = transposon.get_name()
    read_name = transposon.get_read_name()
    finalTE = TE(name, read_name, read2TE_read_start, read2TE_read_end)
    finalTE.set_chr_name(chr_name)
    print()
    finalTE.set_genome_coord(TE2genome_TE_start, TE2genome_TE_end)
    finalTE.set_indicator(indicator)
    return(finalTE)


def parse_reads2TE(sam_path):
    """Parse a sam file with the alignment of reads against a list of consensus TE.
    sam_path (str) : path of the alignment
    Return a dict(id (str), TE list) with all reads and corresponding TE."""

    # Variable

    reads = dict() # id (str), TE list

    # Parsing

    with pysam.AlignmentFile(sam_path, "r") as sam_file:
        for ali in sam_file:
            alig_start = ali.query_alignment_start + 1
            alig_end = ali.query_alignment_end
            read_name = ali.query_name
            element_name = ali.reference_name

            if ali.is_reverse:  # invert start and end to keep the correct order
                read_length = ali.infer_query_length()
                alig_start, alig_end = read_length + 1 - alig_end, read_length + 1 - alig_start

            elements = reads.get(read_name, [])
            element = TE(element_name, read_name, alig_start, alig_end)
            elements.append(element)
            reads[read_name] = elements

    # Return

    return reads

def parse_reads2genome(bam_path, reads):
    """Parse a sam file with the alignment of reads against a genome.
    sam_path (str) : path of the alignment
    reads ((str, TE list) dict) : reads with TE found
    Return a TE list, with all TE with genomic position."""

    # Variable

    elements = list() # TE list
    rev = False

    # Parsing

    with pysam.AlignmentFile(bam_path, "r") as bam_file:

        # Build index
        index = pysam.IndexedReads(bam_file)
        index.build()

        for read in reads.keys():
            try:
                alignments = index.find(read)
                
                for ali in alignments:
                    for element in reads[read]:
                        mapped_element = updateTE(element, ali)  # we update information of the TE
                        elements.append(mapped_element)  # we add the TE to a list of TE

            except KeyError:
                elements.extend(reads[read])  # if the read don't map, its element are add without more information

    return elements

def output_TE(elements, output=None):
    """Print a tabulate information about element.
    elements (TE list) : list of TE to print
    output (str) : if precised, write in the file at the output path """
        
    # Header

    header = ["Read Name origin", "TE Name/Fragment origin", "Chr", "Start", "End", "Indicator"]

    if output is not None:
        o = open(output, "w")
        o.write('\t'.join(header) + '\n')
    else:
        print('\t'.join(header))

    # Lines

    for element in elements:
        Read_Name = element.get_read_name()
        TE_Name = element.name
        Indicator = str(element.get_indicator())
        if Indicator != "0":
            Chr = element.get_chr_name()
            Start = str(element.get_genome_start())
            End = str(element.get_genome_end())
        else:
            Chr = "*"
            Start = "*"
            End = "*"
        info = [Read_Name, TE_Name, Chr, Start, End, Indicator]
        if output is not None:
            o.write('\t'.join(info) + '\n')
        else:
            print('\t'.join(info))
    
    # Close file

    if output is not None:
        o.close()

def output_TE_BED(elements, output=None):
    """Print a bed6 format with score as the indicator.
    elements (TE list) : list of TE to print
    output (str) : if precised, write in the file at the output path"""

    if output is not None:
        o = open(output, "w")

    for element in elements:
        TE_Name = element.name
        Indicator = str(element.get_indicator())
        if Indicator != "0":
            Chr = element.get_chr_name()
            Start = str(element.get_genome_start())
            End = str(element.get_genome_end())
            info = [ Chr, Start, End, TE_Name, Indicator, "."]
            if output is not None:
                o.write('\t'.join(info) + '\n')
            else:
                print('\t'.join(info))
    
    if output is not None:
        o.close()

# Main


if __name__ == "__main__":

    # Parse

    sam_path = args.reads2TE
    bam_path = args.reads2genome

    print("Path of reads2TE : ", sam_path)
    print("Parsing the file to find element by read ...")

    reads = parse_reads2TE(sam_path)
    
    print("Done !\n")

    print("Path of the reads2genome : ", bam_path)

    print("Parsing of the file to find the position of TE in the genome")

    elements = parse_reads2genome(bam_path, reads)

    print("Done !\n")

    if args.output is None:
        print("Output in standard output : \n")
    else:
        print("Output in : ", args.output)

    if args.tabular_format:
        output_TE(elements, args.output)
    else:
        output_TE_BED(elements, args.output)
