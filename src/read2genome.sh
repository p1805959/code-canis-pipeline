#!/bin/bash

set -e 


mkdir -p res/alignment

paftools.js gff2bed res/data/ref/reference_transposons.fa res/data/ref/intron_junction.bed

# Align all LR against the genome with annotation
minimap2 -aYx splice --junc-bed res/data/ref/intron_junction.bed res/data/ref/reference_genome.fa res/data/long_read/Athaliana_LR_X.fastq > res/alignment/all_lr_with_annot_2genome.sam
# ddm1 LR against the genome with annotation
minimap2 -aYx splice --junc-bed res/data/ref/intron_junction.bed res/data/ref/reference_genome.fa res/data/long_read/Athaliana_LR_ddm1.fastq > res/alignment/ddm1_lr_with_annot_2genome.sam
# wt LR against the genome with annotation
minimap2 -aYx splice --junc-bed res/data/ref/intron_junction.bed res/data/ref/reference_genome.fa res/data/long_read/Athaliana_LR_wt.fastq > res/alignment/wt_lr_with_annot_2genome.sam
# dm1-rdr6-polV LR against the genome with annotation
cat res/data/long_read/Athaliana_LR_ddm1-rdr6-polV_rep1.fastq res/data/long_read/Athaliana_LR_ddm1-rdr6-polV_rep2.fastq > res/data/long_read/Athaliana_LR_ddm1-rdr6-polV.fastq
minimap2 -aYx splice --junc-bed res/data/ref/intron_junction.bed res/data/ref/reference_genome.fa res/data/long_read/Athaliana_LR_ddm1-rdr6-polV.fastq > res/alignment/ddm1-rdr6-polV_lr_with_annot_2genome.sam
