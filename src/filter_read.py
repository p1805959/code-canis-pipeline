"""Filter a .sam file and remove unwanted read.
Inspire du script sam2coverage.py de MARY Arnaud et LACROIX Vincent.

!!! No hard-masking allowed"""

import pysam
import argparse


# Shell interface

parser = argparse.ArgumentParser(
                    prog = 'filter_read.py',
                    description = 'Filter a .sam file and remove unwanted read.',
                    epilog = 'For more information, contact pierre.gerenton@etu.univ-lyon1.fr.')

parser.add_argument("samfile",
                    help = "Path of a SAM file.")
parser.add_argument("-k", "--keep", required=False,
                    default="all", type=str,
                    choices=["all","primary","not_supp", "separate"],
                    help="Choose which alignment keep if multiple by read")
# all, keep all alignment
# primary, keep the primary alignment
# not_supp, do not keep supplementary alignment
# separate, keep primary alignment and some other if they don't overlap
parser.add_argument("-q", "--query-coverage", required=False,
                    default=0.2, type=float,
                    help = "Minimum coverage of the query sequence by the alignment.")
parser.add_argument("-r","--reference-coverage", required=False,
                    default=0.8, type=float,
                    help = "Minimum coverage of the query sequence by the alignment.")
parser.add_argument("-m","--maximum-qc", required=False,
                    action="store_true",
                    help = "If this option is chosen, the value of reference-coverage will be the maximum reference coverage"
                    "instead of the minimum")
parser.add_argument("-M","--maximum-rc", required=False,
                    action="store_true",
                    help = "If this option is chosen, the value of query-coverage will be the maximum query coverage"
                    "instead of the minimum")
parser.add_argument("-o","--output-file", required=False,
                    default=None,
                    help = "Path to an output file. If not specified, the output will be write in the standart output.")


args = parser.parse_args()

# Def function

def check_ali(ali, query_coverage, reference_coverage, maximum_qc, maximum_rc):
    """Write ali if the ali is OK."""
    # Query cover
    query_coverage = ali.query_alignment_length / ali.infer_read_length()
    if maximum_qc:
        query_coverage_condition = (query_coverage <= args.query_coverage)
    else:
        query_coverage_condition = (query_coverage >= args.query_coverage)

    # Reference cover
    reference_name = ali.reference_name
    reference_coverage = ali.reference_length / sam_file.get_reference_length(reference_name)
    if maximum_rc:
        reference_coverage_condition = (reference_coverage <= args.reference_coverage)
    else:
        reference_coverage_condition = (reference_coverage >= args.reference_coverage)
    
    return query_coverage_condition and reference_coverage_condition
        

def write_ali(ali, output_file):
    if args.output_file is None:
        print(ali.to_string())
    else:
        output_file.write(ali)

def overlap(ali, listali):
    start_ali = ali.query_alignment_start
    end_ali = ali.query_alignment_end
    for comparaison in listali:
        start_comp = comparaison.query_alignment_start
        end_comp = comparaison.query_alignment_end
        if not ((start_comp > end_ali) or (end_comp < start_ali)):
            return True
    return False

# Main

# Files preparation

sam_file = pysam.AlignmentFile(args.samfile, "r")    

if args.output_file is None:
    print(sam_file.header)
else:
    print("Output file : ",args.output_file)
    output_file = pysam.AlignmentFile(args.output_file, "w", template=sam_file)


# Get alignment by Query Name

reads = dict()
for ali in sam_file:
    read_name = ali.query_name
    if reads.get(read_name):
        reads[read_name].append(ali)
    else:
        reads[read_name] = [ali]


# Filter

for read in reads:
    if args.keep == "separate":
        kept_ali = list()
        for ali in reads[read]:
            if not ali.is_secondary:
                kept_ali.append(ali)
                break
        for ali in reads[read]:
            if not overlap(ali, kept_ali):
                kept_ali.append(ali)
        for ali in kept_ali:
            if check_ali(ali,
                         args.query_coverage,
                         args.reference_coverage,
                         args.maximum_qc,
                         args.maximum_rc):
                write_ali(ali, output_file)

    else:
        for ali in reads[read]:
            if ali.is_supplementary and args.keep == "not_comp":
                continue
            elif ali.is_secondary and args.keep == "primary":
                continue
            elif check_ali(ali,
                         args.query_coverage,
                         args.reference_coverage,
                         args.maximum_qc,
                         args.maximum_rc):
                write_ali(ali, output_file)