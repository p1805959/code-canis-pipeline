#!/bin/bash

set -e

# filter uninteresting read
# we want that :
#    1. the query isn't the transposable element (the query coverage is inferior to a threesold)
#    2. the integrality of the transposable element is present in our alignment (reference coverage is superior to a threesold)

mkdir res/alignment/chimera -p

echo "Filtre testis"

echo "Filtre aston"
python3 src/filter_read.py res/alignment/aston_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/aston_chimera_read.sam -k separate
echo "Filtre domino"
python3 src/filter_read.py res/alignment/domino_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/domino_chimera_read.sam -k separate
echo "Filtre emir"
python3 src/filter_read.py res/alignment/emir_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/emir_chimera_read.sam -k separate
echo "Filtre fute"
python3 src/filter_read.py res/alignment/fute_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/fute_chimera_read.sam -k separate
echo "Filtre samy"
python3 src/filter_read.py res/alignment/samy_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/samy_chimera_read.sam -k separate
echo "Filtre voyou"
python3 src/filter_read.py res/alignment/voyou_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/voyou_chimera_read.sam -k separate

echo "Filtre cancer"

echo "Filtre bear"
python3 src/filter_read.py res/alignment/bear_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/bear_chimera_read.sam -k separate
echo "Filtre chipie"
python3 src/filter_read.py res/alignment/chipie_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/chipie_chimera_read.sam -k separate
echo "Filtre chrystalPoumon"
python3 src/filter_read.py res/alignment/chrystalPoumon_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/chrystalPoumon_chimera_read.sam -k separate
echo "Filtre chrystalRate"
python3 src/filter_read.py res/alignment/chrystalRate_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/chrystalRate_chimera_read.sam -k separate
echo "Filtre cml10"
python3 src/filter_read.py res/alignment/cml10_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/cml10_chimera_read.sam -k separate
echo "Filtre popsi"
python3 src/filter_read.py res/alignment/popsi_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/popsi_chimera_read.sam -k separate
echo "Filtre twiny"
python3 src/filter_read.py res/alignment/twiny_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/twiny_chimera_read.sam -k separate
echo "Filtre vico"
python3 src/filter_read.py res/alignment/vico_reads_with_TE.sam -q 0.5 -r 0.8 -m -o res/alignment/chimera/vico_chimera_read.sam -k separate

