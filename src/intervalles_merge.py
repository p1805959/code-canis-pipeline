""" Fuse overlapping interval.
"""

import pandas as pd
import numpy as np
import argparse

pd.options.mode.chained_assignment = None

# Shell interface

parser = argparse.ArgumentParser(
                    prog = 'intervalles.py',
                    description = 'From the precedent .tsv, create file describing new intervalles.',
                    epilog = 'For more information, contact pierre.gerenton@etu.univ-lyon1.fr.')

parser.add_argument("tsvpath",
                    help = "Path of a .tsv file.")
parser.add_argument("output_prefix", default=None,
                    help = "Prefix of all 3 output files generated.")
parser.add_argument("-i","--index",
                    action="store_true", default=False, required=False,
                    help = "Print a copy of the tsv with a column with index/row number.")


args = parser.parse_args()


# lecture du fichier

data = pd.read_csv(args.tsvpath, sep="\t")

data["Index"]=data.index
data = data[data["Chr"]!="*"]  # on enlève les reads qui ne se retrouve pas sur le génome

data = data.astype({"Start":int, "End":int})

liste_chr = np.unique(data["Chr"])

equivalence = pd.DataFrame(columns=["Chr","Start","End","TE Name","Group","Index"])
intervalles = pd.DataFrame(columns=["Chr","Start","End","TE Name","Group","*"])

duree = len(liste_chr)
i = 0

for chromosome in liste_chr:
    i+=1
    if i%15==0:
        print(i, " / ",duree) 
    df = data[data["Chr"]==chromosome]
    liste_te = np.unique(df["TE Name/Fragment origin"])
    for te in liste_te:
        df_te = df[df["TE Name/Fragment origin"]==te]
        df_te.sort_values("Start", inplace=True)
        df_te["Group"] = (df_te["Start"]>df_te["End"].shift().cummax()).cumsum()
        df_te["Group"] = chromosome + "_" + te + "_" + df_te["Group"].astype(str)
        df_te = df_te.groupby("Group").agg({"Group":lambda x: list(x)[0],"Start":"min","End":"max","Index":list})
        df_te["Chr"] = chromosome
        df_te["TE Name"] = te
        df_te["*"] = "*"

        equivalence = pd.concat([equivalence,df_te[["Chr","Start","End","TE Name","Group","Index"]]])
        intervalles = pd.concat([intervalles,df_te[["Chr","Start","End","TE Name","Group","*"]]])

intervalles["Start"][intervalles["Start"]<0] = 0 # avoid bug
intervalles["End"][intervalles["End"]<0] = 0 # avoid bug

equivalence["Length"] = equivalence["Index"].apply(len)

# Output file
# _indexed.tsv : original csv with index, to retreive important element (only with -i option)
# _equivalence.tsv : give the index of element (row line) of each interval, with the number of element inside
# _intervalles.bed : bigger intervalles, bed format

print("File output :")

if args.index:
    data.to_csv(args.output_prefix + "_indexed.tsv", index=False, sep="\t")
    print(args.output_prefix + "_indexed.tsv is for finding original subinterval with index")
equivalence.to_csv(args.output_prefix + "_merge_equivalence.tsv", index=False, sep="\t")
print(args.output_prefix + "_merge_equivalence.tsv to find the element which make an intervalle")
intervalles.to_csv(args.output_prefix + "_merge_intervalles.bed", header=False, index=False, sep="\t")
print(args.output_prefix + "_merge_intervalles.bed file usefull to represent intervalle feature")
