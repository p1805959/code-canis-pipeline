#!/bin/bash

set -e

# align reads with a list of consensus TE
 
mkdir res/alignment -p

# testicle data

echo "Traitement des données testis ..."

echo "Traitement des données aston ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/testicle/aston.fastq.gz > res/alignment/aston_reads2TE.sam
cat res/alignment/aston_reads2TE.sam | awk '($3!="*")' > res/alignment/aston_reads_with_TE.sam
rm res/alignment/aston_reads2TE.sam
echo "Fait."

echo "Traitement des données domino ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/testicle/domino.fastq.gz > res/alignment/domino_reads2TE.sam
cat res/alignment/domino_reads2TE.sam | awk '($3!="*")' > res/alignment/domino_reads_with_TE.sam
rm res/alignment/domino_reads2TE.sam
echo "Fait."

echo "Traitement des données emir ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/testicle/emir.fastq.gz > res/alignment/emir_reads2TE.sam
cat res/alignment/emir_reads2TE.sam | awk '($3!="*")' > res/alignment/emir_reads_with_TE.sam
rm res/alignment/emir_reads2TE.sam
echo "Fait."

echo "Traitement des données fute ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/testicle/fute.fastq.gz > res/alignment/fute_reads2TE.sam
cat res/alignment/fute_reads2TE.sam | awk '($3!="*")' > res/alignment/fute_reads_with_TE.sam
rm res/alignment/fute_reads2TE.sam
echo "Fait."

echo "Traitement des données samy ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/testicle/samy.fastq.gz > res/alignment/samy_reads2TE.sam
cat res/alignment/samy_reads2TE.sam | awk '($3!="*")' > res/alignment/samy_reads_with_TE.sam
rm res/alignment/samy_reads2TE.sam
echo "Fait."

echo "Traitement des données voyou ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/testicle/voyou.fastq.gz > res/alignment/voyou_reads2TE.sam
cat res/alignment/voyou_reads2TE.sam | awk '($3!="*")' > res/alignment/voyou_reads_with_TE.sam
rm res/alignment/voyou_reads2TE.sam
echo "Fait."

# cancer data

echo "Traitement des données cancer ..."

echo "Traitement des données bear ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/cancer/bear.fastq.gz > res/alignment/bear_reads2TE.sam
cat res/alignment/bear_reads2TE.sam | awk '($3!="*")' > res/alignment/bear_reads_with_TE.sam
rm res/alignment/bear_reads2TE.sam
echo "Fait."

echo "Traitement des données chipie ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/cancer/chipie.fastq.gz > res/alignment/chipie_reads2TE.sam
cat res/alignment/chipie_reads2TE.sam | awk '($3!="*")' > res/alignment/chipie_reads_with_TE.sam
rm res/alignment/chipie_reads2TE.sam
echo "Fait."

echo "Traitement des données chrystalPoumon ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/cancer/chrystalPoumon.fastq.gz > res/alignment/chrystalPoumon_reads2TE.sam
cat res/alignment/chrystalPoumon_reads2TE.sam | awk '($3!="*")' > res/alignment/chrystalPoumon_reads_with_TE.sam
rm res/alignment/chrystalPoumon_reads2TE.sam
echo "Fait."

echo "Traitement des données chrystalRate ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/cancer/chrystalRate.fastq.gz > res/alignment/chrystalRate_reads2TE.sam
cat res/alignment/chrystalRate_reads2TE.sam | awk '($3!="*")' > res/alignment/chrystalRate_reads_with_TE.sam
rm res/alignment/chrystalRate_reads2TE.sam
echo "Fait."

echo "Traitement des données cml10 ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/cancer/cml10.fastq.gz > res/alignment/cml10_reads2TE.sam
cat res/alignment/cml10_reads2TE.sam | awk '($3!="*")' > res/alignment/cml10_reads_with_TE.sam
rm res/alignment/cml10_reads2TE.sam
echo "Fait."

echo "Traitement des données popsi ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/cancer/popsi.fastq.gz > res/alignment/popsi_reads2TE.sam
cat res/alignment/popsi_reads2TE.sam | awk '($3!="*")' > res/alignment/popsi_reads_with_TE.sam
rm res/alignment/popsi_reads2TE.sam
echo "Fait."

echo "Traitement des données twiny ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/cancer/twiny.fastq.gz > res/alignment/twiny_reads2TE.sam
cat res/alignment/twiny_reads2TE.sam | awk '($3!="*")' > res/alignment/twiny_reads_with_TE.sam
rm res/alignment/twiny_reads2TE.sam
echo "Fait."

echo "Traitement des données vico ..."
minimap2 -aYx splice res/data/ref/transposons_consensus.fa res/data/long_read/cancer/vico.fastq.gz > res/alignment/vico_reads2TE.sam
cat res/alignment/vico_reads2TE.sam | awk '($3!="*")' > res/alignment/vico_reads_with_TE.sam
rm res/alignment/vico_reads2TE.sam
echo "Fait."