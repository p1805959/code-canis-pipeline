#!/bin/bash

# Fetch data for Canis lupus familiaris

set -e


# TODO


REF_GENOME="https://homeandco.genouest.org/api/download/groups/dog/data/canFam4/sequence/UU_Cfam_GSD_1.0_canFam4/UU_Cfam_GSD_1.0_ROSY.fa?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7IlVpZCI6InBnZXJlbnRvbiIsIlBhc3N3b3JkIjoiIiwiVWlkTnVtYmVyIjo1OTM1NywiR2lkTnVtYmVyIjo0MDQwMSwiSG9tZSI6Ii9ob21lL2dlbm91ZXN0L2NucnNfdW1yNTU1OC9wZ2VyZW50b24iLCJHcm91cHMiOlt7IkdpZE51bWJlciI6NDA0MDEsIk5hbWUiOiJjbnJzX3VtcjU1NTgifV0sIkFkbWluIjpmYWxzZSwiU2hhcmVzIjpbeyJQYXRoIjoiL2dyb3VwcyIsIlJlYWRXcml0ZSI6dHJ1ZSwiU2hhcmVkQnkiOm51bGwsIkZvcmJpZHMiOm51bGx9XX0sIm5hbWUiOiIiLCJleHAiOjE2ODk3MTYxNTYsImlzcyI6ImhvbWVhbmRjbyJ9.oKsGNSpYTn7N1NhpmvronppnMb_6Hl8OsCZ23y6yzX8"
REF_ANNOTATION="https://homeandco.genouest.org/api/download/groups/dog/data/canFam4/annotation/refseq/refseq_with_chrY.dir/UU_Cfam_GSD_1.0_ROSY.refSeq.with_chr.CurGen.pseudo.gtf?token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7IlVpZCI6InBnZXJlbnRvbiIsIlBhc3N3b3JkIjoiIiwiVWlkTnVtYmVyIjo1OTM1NywiR2lkTnVtYmVyIjo0MDQwMSwiSG9tZSI6Ii9ob21lL2dlbm91ZXN0L2NucnNfdW1yNTU1OC9wZ2VyZW50b24iLCJHcm91cHMiOlt7IkdpZE51bWJlciI6NDA0MDEsIk5hbWUiOiJjbnJzX3VtcjU1NTgifV0sIkFkbWluIjpmYWxzZSwiU2hhcmVzIjpbeyJQYXRoIjoiL2dyb3VwcyIsIlJlYWRXcml0ZSI6dHJ1ZSwiU2hhcmVkQnkiOm51bGwsIkZvcmJpZHMiOm51bGx9XX0sIm5hbWUiOiIiLCJleHAiOjE2ODk3MTYxNTYsImlzcyI6ImhvbWVhbmRjbyJ9.oKsGNSpYTn7N1NhpmvronppnMb_6Hl8OsCZ23y6yzX8"
# # annotation with TE
# REF_TRANSPOSONS=""
# TRANSPOSONS_CONSENSUS=""

#both array has to have the same length
# LR_SRA_ID=()
# LR_LABEL=()


# Fetch reference files

wget -O res/data/ref/reference_genome.fa $REF_GENOME
wget -O res/data/ref/reference_annotation.gff $REF_ANNOTATION
# wget -O res/data/ref/reference_transposons.fa $REF_TRANSPOSONS
# wget -O res/data/ref/transposons_consensus.fa $TRANSPOSONS_CONSENSUS


# # Fetch reads

# for index in ${!LR_SRA_ID[*]};
#     do
#     prefetch -O res/data/long_read ${LR_SRA_ID[$index]};
#     fastq-dump -O res/data/long_read/ res/data/long_read/${LR_SRA_ID[$index]}/${LR_SRA_ID[$index]}.sra;
#     mv res/data/long_read/${LR_SRA_ID[$index]}/${LR_SRA_ID[$index]}.fastq res/data/long_read/${LR_LABEL[$index]};
#     done

eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7IlVpZCI6InBnZXJlbnRvbiIsIlBhc3N3b3JkIjoiIiwiVWlkTnVtYmVyIjo1OTM1NywiR2lkTnVtYmVyIjo0MDQwMSwiSG9tZSI6Ii9ob21lL2dlbm91ZXN0L2NucnNfdW1yNTU1OC9wZ2VyZW50b24iLCJHcm91cHMiOlt7IkdpZE51bWJlciI6NDA0MDEsIk5hbWUiOiJjbnJzX3VtcjU1NTgifV0sIkFkbWluIjpmYWxzZSwiU2hhcmVzIjpbeyJQYXRoIjoiL2dyb3VwcyIsIlJlYWRXcml0ZSI6dHJ1ZSwiU2hhcmVkQnkiOm51bGwsIkZvcmJpZHMiOm51bGx9XX0sIm5hbWUiOiIiLCJleHAiOjE2OTQ0NTU5NDQsImlzcyI6ImhvbWVhbmRjbyJ9.FbWSNIsVXTtPSXNNNsgbcCPoh5zjBRA_2lXJY9K6dtY

TOKEN="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7IlVpZCI6InBnZXJlbnRvbiIsIlBhc3N3b3JkIjoiIiwiVWlkTnVtYmVyIjo1OTM1NywiR2lkTnVtYmVyIjo0MDQwMSwiSG9tZSI6Ii9ob21lL2dlbm91ZXN0L2NucnNfdW1yNTU1OC9wZ2VyZW50b24iLCJHcm91cHMiOlt7IkdpZE51bWJlciI6NDA0MDEsIk5hbWUiOiJjbnJzX3VtcjU1NTgifV0sIkFkbWluIjpmYWxzZSwiU2hhcmVzIjpbeyJQYXRoIjoiL2dyb3VwcyIsIlJlYWRXcml0ZSI6dHJ1ZSwiU2hhcmVkQnkiOm51bGwsIkZvcmJpZHMiOm51bGx9XX0sIm5hbWUiOiIiLCJleHAiOjE2OTQ0NTU5NDQsImlzcyI6ImhvbWVhbmRjbyJ9.FbWSNIsVXTtPSXNNNsgbcCPoh5zjBRA_2lXJY9K6dtY"

mkdir -p res/data/long_read/cancer
mkdir -p res/data/long_read/testicle

# Long Read (Cancer) direct cDNA

wget -O res/data/long_read/cancer/bear.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/Bear/cdna_SQK-DCS109/flowcell/guppy_6.0.0/Bear_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/cancer/chipie.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/Chipie/cdna_SQK-DCS109/flowcell/guppy_6.0.0/Chipie_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/cancer/chrystalPoumon.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/chrystalPoumon/cdna_SQK-DCS109/flowcell/guppy_6.0.0/chrystalPoumon_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/cancer/chrystalRate.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/chrystalRate/cdna_SQK-DCS109/flowcell/guppy_6.0.0/chrystalRate_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/cancer/cml10.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/CML10/cdna_SQK-DCS109/flowcell/guppy_6.0.0/CML10_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/cancer/popsi.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/popsi/cdna_SQK-DCS109/flowcell/guppy_6.0.0/popsi_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/cancer/twiny.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/twiny/cdna_SQK-DCS109/flowcell/guppy_6.0.0/twiny_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/cancer/vico.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/vico/cdna_SQK-DCS109/flowcell/guppy_6.0.0/vico_R1.fastq.gz?token=${TOKEN}"

# Long Read (Testicles) direct RNA

wget -O res/data/long_read/testicle/aston.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Aston/drna_SQK-RNA002/flowcell/guppy_4.0.14/Aston_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/testicle/domino.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Domino/drna_SQK-RNA002/flowcell/guppy_4.0.14/Domino_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/testicle/emir.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Emir/drna_SQK-RNA002/flowcell/guppy_4.0.14/Emir_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/testicle/fute.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Fute/drna_SQK-RNA002/flowcell/guppy_4.0.14/Fute_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/testicle/samy.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Samy/drna_SQK-RNA002/flowcell/guppy_4.0.14/Samy_R1.fastq.gz?token=${TOKEN}"
wget -O res/data/long_read/testicle/voyou.fastq.gz "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Voyou/drna_SQK-RNA002/flowcell/guppy_4.0.14/Voyou_R1.fastq.gz?token=${TOKEN}"


mkdir -p res/data/bam/cancer
mkdir -p res/data/bam/testicle

# BAM File Cancer

wget -O res/data/bam/cancer/bear.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/Bear/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/Bear_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/cancer/bear.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/Bear/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/Bear_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/cancer/chipie.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/Chipie/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/Chipie_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/cancer/chipie.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/Chipie/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/Chipie_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/cancer/chrystalPoumon.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/chrystalPoumon/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/chrystalPoumon_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/cancer/chrystalPoumon.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/chrystalPoumon/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/chrystalPoumon_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/cancer/chrystalRate.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/chrystalRate/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/chrystalRate_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/cancer/chrystalRate.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/chrystalRate/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/chrystalRate_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/cancer/cml10.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/CML10/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/CML10_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/cancer/cml10.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/CML10/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/CML10_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/cancer/popsi.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/popsi/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/popsi_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/cancer/popsi.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/popsi/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/popsi_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/cancer/twiny.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/twiny/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/twiny_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/cancer/twiny.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/twiny/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/twiny_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/cancer/vico.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/vico/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/vico_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/cancer/vico.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/lr_cancercell/primary/dog/vico/cdna_SQK-DCS109/flowcell/guppy_6.0.0/bam/canFam4/bam/vico_R1.sorted.bam.bai?token=${TOKEN}"


# BAM (Testicles) direct RNA

wget -O res/data/bam/testicle/aston.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Aston/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Aston_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/testicle/aston.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Aston/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Aston_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/testicle/domino.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Domino/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Domino_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/testicle/domino.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Domino/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Domino_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/testicle/emir.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Emir/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Emir_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/testicle/emir.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Emir/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Emir_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/testicle/fute.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Fute/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Fute_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/testicle/fute.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Fute/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Fute_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/testicle/samy.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Samy/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Samy_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/testicle/samy.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Samy/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Samy_R1.sorted.bam.bai?token=${TOKEN}"
wget -O res/data/bam/testicle/voyou.bam "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Voyou/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Voyou_R1.sorted.bam?token=${TOKEN}"
wget -O res/data/bam/testicle/voyou.bam.bai "https://homeandco.genouest.org/api/download/groups/dog/nanopore/anr_instead/primary/dog/Voyou/drna_SQK-RNA002/flowcell/guppy_4.0.14/bam/canFam4/Voyou_R1.sorted.bam.bai?token=${TOKEN}"