"Return a bed file of Intervalle instead of transposons"

import argparse

# Shell interface

parser = argparse.ArgumentParser(
                    prog = 'Intervalle.py',
                    description = 'Take a tabular file and return intervalle when multiple features overlap. Return a bed file.',
                    epilog = 'For more information, contact pierre.gerenton@etu.univ-lyon1.fr.')

parser.add_argument("tab_file",
                    help = "Path of the tab file. \n")
parser.add_argument("output",
                    default=None,
                    help="Path of the output file (without extention)")

args = parser.parse_args()


# Class

class Intervalle:
    """Represent an Intervalle."""

    # Constructor

    def __init__(self, id, transposon):
        """Create an Intervalle object.
        id (int) : number of the intervalle
        chr (str) : chromosome
        te_name (str) : name of the transposon
        transposon [read, start, end] : first transposon of the intervalle
        """
        self.id = id
        self.start = transposon[1]
        self.end = transposon[2]
        self.transposons = [transposon]

    # Getters

    def get_start(self):
        return self.start
    def get_end(self):
        return self.end
    
    def get_id(self):
        return self.id
    def get_transposons(self):
        return self.transposons
    

    # Methods

    def is_in(self, transposon):
        """
        transposon [read, start, end]
        """
        transposon[1] < self.end and transposon[2] > self.start

    def add_transposon(self, transposon):
        """
        transposon [read, start, end]
        """
        self.start = min(self.start, transposon[1])
        self.end = max(self.end, transposon[2])
        self.transposons.append(transposon)



# Function

def read_tab(path):
    """
    Parse a tab file with :
    read_name te_name chr start end score
    """
    intervalles = dict()
    id = 1

    with open(path) as tab:
        for line in tab:
            element = line.split('\t')
            chr = element[2]
            te_name = element[1]
            transposon = [element[0], element[3], element[4]]
            if not chr in intervalles:
                intervalles[chr] = {te_name:[Intervalle(id, transposon)]}
                id += 1
            elif not te_name in intervalles[chr]:
                intervalles[chr][te_name] = [Intervalle(id, transposon)]
                id += 1
            else:
                for intervalle in intervalles[chr][te_name]:
                    if intervalle.is_in(transposon):
                        intervalle.add_transposon(transposon)
                    else:
                        intervalles[chr][te_name].append(Intervalle(id, transposon))
                        id += 1

    return intervalles


def write_outputs(intervalles, path):
    """
    Write simultenaously the two output.
    """

    with open(path + ".bed", "w") as bedfile:
        with open(path + "_intervalle_info.txt", "w") as infofile:
            for chr in intervalles:
                for te_name in intervalles[chr]:
                    for intervalle in intervalles[chr][te_name]:
                        start = intervalle.get_start()
                        end = intervalle.get_end()
                        id = intervalle.get_id()
                        transposons = intervalle.get_transposons()
                        bedline = "\t".join([chr, str(start), str(end), id, '.', '.'])
                        bedfile.write(bedline + '\n')
                        infofile.write(bedline+"\n------------")
                        for transposon in transposon:
                            infofile.write(transposon + "\n")
                        infofile.write("\n")


# Main


if __name__ == "__main__":

    # Parse

    path = args.tab_file

    intervalles = read_tab(path)

    output = args.output

    write_outputs(intervalles, output)
    