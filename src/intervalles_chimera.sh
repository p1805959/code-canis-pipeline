#!/bin/bash

set -e

echo "Merge testis"
head -n 1 res/annotation/chimera/aston_chimera_genome.tsv > res/annotation/chimera/testis_chimera_genome.tsv
tail -qn +2 res/annotation/chimera/aston_chimera_genome.tsv res/annotation/chimera/domino_chimera_genome.tsv res/annotation/chimera/emir_chimera_genome.tsv res/annotation/chimera/fute_chimera_genome.tsv res/annotation/chimera/samy_chimera_genome.tsv res/annotation/chimera/voyou_chimera_genome.tsv >> res/annotation/chimera/testis_chimera_genome.tsv
cat res/annotation/chimera/aston_chimera_genome.bed res/annotation/chimera/domino_chimera_genome.bed res/annotation/chimera/emir_chimera_genome.bed res/annotation/chimera/fute_chimera_genome.bed res/annotation/chimera/samy_chimera_genome.bed res/annotation/chimera/voyou_chimera_genome.bed > res/annotation/chimera/testis_chimera_genome.bed

echo "Merge cancer"
head -n 1 res/annotation/chimera/bear_chimera_genome.tsv > res/annotation/chimera/cancer_chimera_genome.tsv
tail -qn +2 res/annotation/chimera/bear_chimera_genome.tsv res/annotation/chimera/chipie_chimera_genome.tsv res/annotation/chimera/chrystalPoumon_chimera_genome.tsv res/annotation/chimera/chrystalRate_chimera_genome.tsv res/annotation/chimera/cml10_chimera_genome.tsv res/annotation/chimera/popsi_chimera_genome.tsv res/annotation/chimera/twiny_chimera_genome.tsv res/annotation/chimera/vico_chimera_genome.tsv >> res/annotation/chimera/cancer_chimera_genome.tsv
cat res/annotation/chimera/bear_chimera_genome.bed res/annotation/chimera/chipie_chimera_genome.bed res/annotation/chimera/chrystalPoumon_chimera_genome.bed res/annotation/chimera/chrystalRate_chimera_genome.bed res/annotation/chimera/cml10_chimera_genome.bed res/annotation/chimera/popsi_chimera_genome.bed res/annotation/chimera/twiny_chimera_genome.bed res/annotation/chimera/vico_chimera_genome.bed > res/annotation/chimera/cancer_chimera_genome.bed


echo "Testis :"

python3 ./src/intervalles_merge.py res/annotation/chimera/testis_chimera_genome.tsv ./res/annotation/chimera/testis_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/testis_chimera_genome.tsv ./res/annotation/chimera/testis_chimera_genome -p 1000

echo "aston :"
python3 ./src/intervalles_merge.py res/annotation/chimera/aston_chimera_genome.tsv ./res/annotation/chimera/aston_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/aston_chimera_genome.tsv ./res/annotation/chimera/aston_chimera_genome -p 1000
echo "domino :"
python3 ./src/intervalles_merge.py res/annotation/chimera/domino_chimera_genome.tsv ./res/annotation/chimera/domino_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/domino_chimera_genome.tsv ./res/annotation/chimera/domino_chimera_genome -p 1000
echo "emir :"
python3 ./src/intervalles_merge.py res/annotation/chimera/emir_chimera_genome.tsv ./res/annotation/chimera/emir_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/emir_chimera_genome.tsv ./res/annotation/chimera/emir_chimera_genome -p 1000
echo "fute :"
python3 ./src/intervalles_merge.py res/annotation/chimera/fute_chimera_genome.tsv ./res/annotation/chimera/fute_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/fute_chimera_genome.tsv ./res/annotation/chimera/fute_chimera_genome -p 1000
echo "samy :"
python3 ./src/intervalles_merge.py res/annotation/chimera/samy_chimera_genome.tsv ./res/annotation/chimera/samy_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/samy_chimera_genome.tsv ./res/annotation/chimera/samy_chimera_genome -p 1000
echo "voyou :"
python3 ./src/intervalles_merge.py res/annotation/chimera/voyou_chimera_genome.tsv ./res/annotation/chimera/voyou_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/voyou_chimera_genome.tsv ./res/annotation/chimera/voyou_chimera_genome -p 1000


echo "Cancer :"

python3 ./src/intervalles_merge.py res/annotation/chimera/cancer_chimera_genome.tsv ./res/annotation/chimera/cancer_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/cancer_chimera_genome.tsv ./res/annotation/chimera/cancer_chimera_genome -p 1000
# cluster seems not to work

echo "bear :"
python3 ./src/intervalles_merge.py res/annotation/chimera/bear_chimera_genome.tsv ./res/annotation/chimera/bear_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/bear_chimera_genome.tsv ./res/annotation/chimera/bear_chimera_genome -p 1000
echo "chipie :"
python3 ./src/intervalles_merge.py res/annotation/chimera/chipie_chimera_genome.tsv ./res/annotation/chimera/chipie_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/chipie_chimera_genome.tsv ./res/annotation/chimera/chipie_chimera_genome -p 1000
echo "chrystalPoumon :"
python3 ./src/intervalles_merge.py res/annotation/chimera/chrystalPoumon_chimera_genome.tsv ./res/annotation/chimera/chrystalPoumon_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/chrystalPoumon_chimera_genome.tsv ./res/annotation/chimera/chrystalPoumon_chimera_genome -p 1000
# cluster seems not to work
echo "chrystalRate :"
python3 ./src/intervalles_merge.py res/annotation/chimera/chrystalRate_chimera_genome.tsv ./res/annotation/chimera/chrystalRate_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/chrystalRate_chimera_genome.tsv ./res/annotation/chimera/chrystalRate_chimera_genome -p 1000
echo "cml10 :"
python3 ./src/intervalles_merge.py res/annotation/chimera/cml10_chimera_genome.tsv ./res/annotation/chimera/cml10_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/cml10_chimera_genome.tsv ./res/annotation/chimera/cml10_chimera_genome -p 1000
echo "popsi :"
python3 ./src/intervalles_merge.py res/annotation/chimera/popsi_chimera_genome.tsv ./res/annotation/chimera/popsi_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/popsi_chimera_genome.tsv ./res/annotation/chimera/popsi_chimera_genome -p 1000
echo "twiny :"
python3 ./src/intervalles_merge.py res/annotation/chimera/twiny_chimera_genome.tsv ./res/annotation/chimera/twiny_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/twiny_chimera_genome.tsv ./res/annotation/chimera/twiny_chimera_genome -p 1000
echo "vico :"
python3 ./src/intervalles_merge.py res/annotation/chimera/vico_chimera_genome.tsv ./res/annotation/chimera/vico_chimera_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chimera/vico_chimera_genome.tsv ./res/annotation/chimera/vico_chimera_genome -p 1000
