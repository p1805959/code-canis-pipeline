#!/bin/bash

set -e

mkdir -p res/annotation
mkdir -p res/annotation/chimera/chimera

echo "Testis"

echo "aston"
python3 ./src/TEatPos.py ./res/alignment/chimera/aston_chimera_read.sam ./res/data/bam/testicle/aston_primary.bam -to res/annotation/chimera/aston_chimera_genome
echo "Fait."
echo "domino"
python3 ./src/TEatPos.py ./res/alignment/chimera/domino_chimera_read.sam ./res/data/bam/testicle/domino_primary.bam -to res/annotation/chimera/domino_chimera_genome
echo "Fait."
echo "emir"
python3 ./src/TEatPos.py ./res/alignment/chimera/emir_chimera_read.sam ./res/data/bam/testicle/emir_primary.bam -to res/annotation/chimera/emir_chimera_genome
echo "Fait."
echo "fute"
python3 ./src/TEatPos.py ./res/alignment/chimera/fute_chimera_read.sam ./res/data/bam/testicle/fute_primary.bam -to res/annotation/chimera/fute_chimera_genome
echo "Fait."
echo "samy"
python3 ./src/TEatPos.py ./res/alignment/chimera/samy_chimera_read.sam ./res/data/bam/testicle/samy_primary.bam -to res/annotation/chimera/samy_chimera_genome
echo "Fait."
echo "voyou"
python3 ./src/TEatPos.py ./res/alignment/chimera/voyou_chimera_read.sam ./res/data/bam/testicle/voyou_primary.bam -to res/annotation/chimera/voyou_chimera_genome
echo "Fait."

echo "Cancer"

echo "bear"
python3 ./src/TEatPos.py ./res/alignment/chimera/bear_chimera_read.sam ./res/data/bam/cancer/bear_primary.bam -to res/annotation/chimera/bear_chimera_genome
echo "Fait."
echo "chipie"
python3 ./src/TEatPos.py ./res/alignment/chimera/chipie_chimera_read.sam ./res/data/bam/cancer/chipie_primary.bam -to res/annotation/chimera/chipie_chimera_genome
echo "Fait."
echo "chrystalPoumon"
python3 ./src/TEatPos.py ./res/alignment/chimera/chrystalPoumon_chimera_read.sam ./res/data/bam/cancer/chrystalPoumon_primary.bam -to res/annotation/chimera/chrystalPoumon_chimera_genome
echo "Fait."
echo "chrystalRate"
python3 ./src/TEatPos.py ./res/alignment/chimera/chrystalRate_chimera_read.sam ./res/data/bam/cancer/chrystalRate_primary.bam -to res/annotation/chimera/chrystalRate_chimera_genome
echo "Fait."
echo "cml10"
python3 ./src/TEatPos.py ./res/alignment/chimera/cml10_chimera_read.sam ./res/data/bam/cancer/cml10_primary.bam -to res/annotation/chimera/cml10_chimera_genome
echo "Fait."
echo "popsi"
python3 ./src/TEatPos.py ./res/alignment/chimera/popsi_chimera_read.sam ./res/data/bam/cancer/popsi_primary.bam -to res/annotation/chimera/popsi_chimera_genome
echo "Fait."
echo "twiny"
python3 ./src/TEatPos.py ./res/alignment/chimera/twiny_chimera_read.sam ./res/data/bam/cancer/twiny_primary.bam -to res/annotation/chimera/twiny_chimera_genome
echo "Fait."
echo "vico"
python3 ./src/TEatPos.py ./res/alignment/chimera/vico_chimera_read.sam ./res/data/bam/cancer/vico_primary.bam -to res/annotation/chimera/vico_chimera_genome
echo "Fait."
