#!/bin/bash

set -e

mkdir -p res/annotation

echo "Testis"

echo "aston"
python3 ./src/TEatPos.py ./res/alignment/aston_filtered_read.sam ./res/data/bam/testicle/aston_primary.bam -to res/annotation/aston_transposon_genome
echo "Fait."
echo "domino"
python3 ./src/TEatPos.py ./res/alignment/domino_filtered_read.sam ./res/data/bam/testicle/domino_primary.bam -to res/annotation/domino_transposon_genome
echo "Fait."
echo "emir"
python3 ./src/TEatPos.py ./res/alignment/emir_filtered_read.sam ./res/data/bam/testicle/emir_primary.bam -to res/annotation/emir_transposon_genome
echo "Fait."
echo "fute"
python3 ./src/TEatPos.py ./res/alignment/fute_filtered_read.sam ./res/data/bam/testicle/fute_primary.bam -to res/annotation/fute_transposon_genome
echo "Fait."
echo "samy"
python3 ./src/TEatPos.py ./res/alignment/samy_filtered_read.sam ./res/data/bam/testicle/samy_primary.bam -to res/annotation/samy_transposon_genome
echo "Fait."
echo "voyou"
python3 ./src/TEatPos.py ./res/alignment/voyou_filtered_read.sam ./res/data/bam/testicle/voyou_primary.bam -to res/annotation/voyou_transposon_genome
echo "Fait."

echo "Cancer"

echo "bear"
python3 ./src/TEatPos.py ./res/alignment/bear_filtered_read.sam ./res/data/bam/cancer/bear_primary.bam -to res/annotation/bear_transposon_genome
echo "Fait."
echo "chipie"
python3 ./src/TEatPos.py ./res/alignment/chipie_filtered_read.sam ./res/data/bam/cancer/chipie_primary.bam -to res/annotation/chipie_transposon_genome
echo "Fait."
echo "chrystalPoumon"
python3 ./src/TEatPos.py ./res/alignment/chrystalPoumon_filtered_read.sam ./res/data/bam/cancer/chrystalPoumon_primary.bam -to res/annotation/chrystalPoumon_transposon_genome
echo "Fait."
echo "chrystalRate"
python3 ./src/TEatPos.py ./res/alignment/chrystalRate_filtered_read.sam ./res/data/bam/cancer/chrystalRate_primary.bam -to res/annotation/chrystalRate_transposon_genome
echo "Fait."
echo "cml10"
python3 ./src/TEatPos.py ./res/alignment/cml10_filtered_read.sam ./res/data/bam/cancer/cml10_primary.bam -to res/annotation/cml10_transposon_genome
echo "Fait."
echo "popsi"
python3 ./src/TEatPos.py ./res/alignment/popsi_filtered_read.sam ./res/data/bam/cancer/popsi_primary.bam -to res/annotation/popsi_transposon_genome
echo "Fait."
echo "twiny"
python3 ./src/TEatPos.py ./res/alignment/twiny_filtered_read.sam ./res/data/bam/cancer/twiny_primary.bam -to res/annotation/twiny_transposon_genome
echo "Fait."
echo "vico"
python3 ./src/TEatPos.py ./res/alignment/vico_filtered_read.sam ./res/data/bam/cancer/vico_primary.bam -to res/annotation/vico_transposon_genome
echo "Fait."
