mkdir -p res/intervalles/FL/cluster res/intervalles/FL/merge res/intervalles/chimera/cluster res/intervalles/chimera/merge

for species in aston emir bear fute cancer popsi chipie samy chrystalPoumon testis chrystalRate twiny cml10 vico domino voyou
do
   echo Processing $species "(FL/cluster) (path :" $path ")">&2
   path="res/annotation/"$species"_transposon_genome_cluster_intervalles.bed"
   cat $path | sort -t "	" -nrk 6 | awk -F '	' '{print $1"	"$2"	"$3"	"$6}' > res/intervalles/FL/cluster/$species"_FL_cluster_sorted.bed"
   echo Done with $species >&2 
done

for species in aston emir bear fute popsi chipie samy testis chrystalRate twiny cml10 vico domino voyou
do
   echo Processing $species "(chimera/cluster) (path :" $path ")">&2
   path="res/annotation/chimera/"$species"_chimera_genome_cluster_intervalles.bed"
   cat $path | sort -t "	" -nrk 6 | awk -F '	' '{print $1"	"$2"	"$3"	"$6}' > res/intervalles/chimera/cluster/$species"_chimera_cluster_sorted.bed"
   echo Done with $species >&2 
done

for species in aston emir bear fute cancer popsi chipie samy chrystalPoumon testis chrystalRate twiny cml10 vico domino voyou
do
   echo Processing $species "(FL/merge) (path :" $path ")">&2
   path="res/annotation/"$species"_transposon_genome_merge_equivalence.tsv"
   cat $path | sort -t "	" -nrk 7 | awk -F '	' '{print $1"	"$2"	"$3"	"$5"	"$7}' > res/intervalles/FL/merge/$species"_FL_merge_sorted.bed"
   echo Done with $species >&2 
done

for species in aston emir bear fute cancer popsi chipie samy chrystalPoumon testis chrystalRate twiny cml10 vico domino voyou
do
   echo Processing $species "(chimera/merge) (path :" $path ")">&2
   path="res/annotation/chimera/"$species"_chimera_genome_merge_equivalence.tsv"
   cat $path | sort -t "	" -nrk 7 | awk -F '	' '{print $1"	"$2"	"$3"	"$5"	"$7}' > res/intervalles/chimera/merge/$species"_chimera_merge_sorted.bed"
   echo Done with $species >&2 
done