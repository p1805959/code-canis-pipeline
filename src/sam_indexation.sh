#!/bin/bash

set -e 

# NO WHITESPACE IN FILENAME 
# no ".sam"
FILE_TO_INDEX=("all_lr_with_annot_2genome" "ddm1_lr_with_annot_2genome" "wt_lr_with_annot_2genome" "ddm1-rdr6-polV_lr_with_annot_2genome")

for file in ${FILE_TO_INDEX[*]};
    do
    samtools sort res/alignment/$file.sam > res/alignment/$file.sorted.bam
    samtools index res/alignment/$file.sorted.bam;
    done;