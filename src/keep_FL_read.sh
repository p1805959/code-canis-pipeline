#!/bin/bash

set -e

# filter uninteresting read
# we want Full Lenght Transcript with TE

echo "Filtre testis"

echo "Filtre aston"
python3 src/filter_read.py res/alignment/aston_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/aston_filtered_read.sam -k separate
echo "Filtre domino"
python3 src/filter_read.py res/alignment/domino_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/domino_filtered_read.sam -k separate
echo "Filtre emir"
python3 src/filter_read.py res/alignment/emir_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/emir_filtered_read.sam -k separate
echo "Filtre fute"
python3 src/filter_read.py res/alignment/fute_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/fute_filtered_read.sam -k separate
echo "Filtre samy"
python3 src/filter_read.py res/alignment/samy_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/samy_filtered_read.sam -k separate
echo "Filtre voyou"
python3 src/filter_read.py res/alignment/voyou_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/voyou_filtered_read.sam -k separate

echo "Filtre cancer"

echo "Filtre bear"
python3 src/filter_read.py res/alignment/bear_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/bear_filtered_read.sam -k separate
echo "Filtre chipie"
python3 src/filter_read.py res/alignment/chipie_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/chipie_filtered_read.sam -k separate
echo "Filtre chrystalPoumon"
python3 src/filter_read.py res/alignment/chrystalPoumon_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/chrystalPoumon_filtered_read.sam -k separate
echo "Filtre chrystalRate"
python3 src/filter_read.py res/alignment/chrystalRate_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/chrystalRate_filtered_read.sam -k separate
echo "Filtre cml10"
python3 src/filter_read.py res/alignment/cml10_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/cml10_filtered_read.sam -k separate
echo "Filtre popsi"
python3 src/filter_read.py res/alignment/popsi_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/popsi_filtered_read.sam -k separate
echo "Filtre twiny"
python3 src/filter_read.py res/alignment/twiny_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/twiny_filtered_read.sam -k separate
echo "Filtre vico"
python3 src/filter_read.py res/alignment/vico_reads_with_TE.sam -q 0.85 -r 0.85 -o res/alignment/vico_filtered_read.sam -k separate
