for file in aston_transposon_genome.tsv emir_transposon_genome.tsv bear_transposon_genome.tsv fute_transposon_genome.tsv cancer_transposon_genome.tsv popsi_transposon_genome.tsv chipie_transposon_genome.tsv samy_transposon_genome.tsv chrystalPoumon_transposon_genome.tsv testis_transposon_genome.tsv chrystalRate_transposon_genome.tsv twiny_transposon_genome.tsv cml10_transposon_genome.tsv vico_transposon_genome.tsv domino_transposon_genome.tsv voyou_transposon_genome.tsv
do
   path="res/annotation/"$file
   echo Processing $file "(path :" $path ")">&2
   echo $file "(path :" $path ")"
   cat $path | awk -F"	" '($3!="*")' | awk '{print $2}' | sort | uniq | wc -l
   cat $path | awk -F"	" '($3!="*")' | awk '{print $2}' | sort | uniq -c | sort -nrk 1
   echo " "
   echo Done with $file >&2 
done