for file in aston_filtered_read.sam  chrystalRate_filtered_read.sam  fute_filtered_read.sam   vico_filtered_read.sam  bear_filtered_read.sam  cml10_filtered_read.sam  popsi_filtered_read.sam  voyou_filtered_read.sam  chipie_filtered_read.sam   domino_filtered_read.sam   samy_filtered_read.sam  chrystalPoumon_filtered_read.sam  emir_filtered_read.sam  twiny_filtered_read.sam
do
   path="res/alignment/"$file
   echo Processing $file "(path :" $path ")">&2
   echo $file "(path :" $path ")"
   samtools view $path | awk '{print $3}' | sort | uniq | wc -l
   samtools view $path | awk '{print $3}' | sort | uniq -c | sort -nrk 1
   echo " "
   echo Done with $file >&2 
done