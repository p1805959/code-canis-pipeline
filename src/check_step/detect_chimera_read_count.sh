for file in aston_chimera_genome.tsv emir_chimera_genome.tsv bear_chimera_genome.tsv fute_chimera_genome.tsv cancer_chimera_genome.tsv popsi_chimera_genome.tsv chipie_chimera_genome.tsv samy_chimera_genome.tsv chrystalPoumon_chimera_genome.tsv testis_chimera_genome.tsv chrystalRate_chimera_genome.tsv twiny_chimera_genome.tsv cml10_chimera_genome.tsv vico_chimera_genome.tsv domino_chimera_genome.tsv voyou_chimera_genome.tsv
do
   path="res/annotation/chimera/"$file
   echo Processing $file "(path :" $path ")">&2
   echo $file "(path :" $path ")"
   LINE_COUNT=$(cat $path | awk -F"	" '($3!="*")' | wc -l)
   echo "Number of line:" $LINE_COUNT
   READ_COUNT=$(cat $path | awk -F"	" '($3!="*")' | awk '{print $1}' | sort | uniq | wc -l)
   echo "Number of read:" $READ_COUNT
   echo Done with $file >&2 
done