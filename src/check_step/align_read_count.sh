for file in aston_reads_with_TE.sam  chrystalRate_reads_with_TE.sam  fute_reads_with_TE.sam   vico_reads_with_TE.sam  bear_reads_with_TE.sam  cml10_reads_with_TE.sam  popsi_reads_with_TE.sam  voyou_reads_with_TE.sam  chipie_reads_with_TE.sam   domino_reads_with_TE.sam   samy_reads_with_TE.sam  chrystalPoumon_reads_with_TE.sam  emir_reads_with_TE.sam  twiny_reads_with_TE.sam
do
   path="res/alignment/"$file
   echo Processing $file "(path :" $path ")">&2
   echo $file "(path :" $path ")"
   LINE_COUNT=$(samtools view $path | wc -l)
   echo "Number of line:" $LINE_COUNT
   READ_COUNT=$(samtools view $path | awk '{print $1}' | sort | uniq | wc -l)
   echo "Number of read:" $READ_COUNT
   echo Done with $file >&2 
done