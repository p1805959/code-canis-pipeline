for file in aston.fastq.gz  domino.fastq.gz  emir.fastq.gz  fute.fastq.gz  samy.fastq.gz  voyou.fastq.gz
do
   path="res/data/long_read/testicle/"$file
   echo Processing $file "(path :" $path ")">&2
   echo $file "(path :" $path ")"
   LINE_COUNT=$(zcat $path | wc -l)
   echo "Number of line:" $LINE_COUNT
   READ_COUNT=$(expr $LINE_COUNT "/" 4)
   echo "Number of read:" $READ_COUNT
   echo Done with $file >&2 
done

for file in bear.fastq.gz    chrystalPoumon.fastq.gz  cml10.fastq.gz  twiny.fastq.gz chipie.fastq.gz  chrystalRate.fastq.gz    popsi.fastq.gz  vico.fastq.gz
do
   path="res/data/long_read/cancer/"$file
   echo Processing $file "(path :" $path ")">&2
   echo $file "(path :" $path ")"
   LINE_COUNT=$(zcat $path | wc -l)
   echo "Number of line:" $LINE_COUNT
   READ_COUNT=$(expr $LINE_COUNT "/" 4)
   echo "Number of read:" $READ_COUNT
   echo Done with $file >&2 
done