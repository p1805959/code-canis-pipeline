for file in aston_chimera_read.sam  chrystalRate_chimera_read.sam  fute_chimera_read.sam   vico_chimera_read.sam  bear_chimera_read.sam  cml10_chimera_read.sam  popsi_chimera_read.sam  voyou_chimera_read.sam  chipie_chimera_read.sam   domino_chimera_read.sam   samy_chimera_read.sam  chrystalPoumon_chimera_read.sam  emir_chimera_read.sam  twiny_chimera_read.sam
do
   path="res/alignment/chimera/"$file
   echo Processing $file "(path :" $path ")">&2
   echo $file "(path :" $path ")"
   LINE_COUNT=$(samtools view $path | wc -l)
   echo "Number of line:" $LINE_COUNT
   READ_COUNT=$(samtools view $path | awk '{print $1}' | sort | uniq | wc -l)
   echo "Number of read:" $READ_COUNT
   echo Done with $file >&2 
done