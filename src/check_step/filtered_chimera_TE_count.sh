for file in aston_chimera_read.sam  chrystalRate_chimera_read.sam  fute_chimera_read.sam   vico_chimera_read.sam  bear_chimera_read.sam  cml10_chimera_read.sam  popsi_chimera_read.sam  voyou_chimera_read.sam  chipie_chimera_read.sam   domino_chimera_read.sam   samy_chimera_read.sam  chrystalPoumon_chimera_read.sam  emir_chimera_read.sam  twiny_chimera_read.sam
do
   path="res/alignment/chimera/"$file
   echo Processing $file "(path :" $path ")">&2
   echo $file "(path :" $path ")"
   samtools view $path | awk '{print $3}' | sort | uniq | wc -l
   samtools view $path | awk '{print $3}' | sort | uniq -c | sort -nrk 1
   echo " "
   echo Done with $file >&2 
done