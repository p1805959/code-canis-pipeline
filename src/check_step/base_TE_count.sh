file=transposons_consensus.fa
path="res/data/ref/"$file

echo Counting TE in $file "(path :" $path ")">&2
echo $file "(path :" $path ")"
grep ">" $path | wc -l
echo Counting line by category in $file "(path :" $path ")">&2
grep ">" $path | cut -d$'\t' -f 2 | sort | uniq -c | sort -nrk 1
echo Done with $file >&2