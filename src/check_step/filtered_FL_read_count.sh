for file in aston_filtered_read.sam  chrystalRate_filtered_read.sam  fute_filtered_read.sam   vico_filtered_read.sam  bear_filtered_read.sam  cml10_filtered_read.sam  popsi_filtered_read.sam  voyou_filtered_read.sam  chipie_filtered_read.sam   domino_filtered_read.sam   samy_filtered_read.sam  chrystalPoumon_filtered_read.sam  emir_filtered_read.sam  twiny_filtered_read.sam
do
   path="res/alignment/"$file
   echo Processing $file "(path :" $path ")">&2
   echo $file "(path :" $path ")"
   LINE_COUNT=$(samtools view $path | wc -l)
   echo "Number of line:" $LINE_COUNT
   READ_COUNT=$(samtools view $path | awk '{print $1}' | sort | uniq | wc -l)
   echo "Number of read:" $READ_COUNT
   echo Done with $file >&2 
done