détail et ordre de mini script pour vérifier les résultats

### Jeu de données

Comptage long-read
```sh
./src/check_step/base_read_count.sh > res/verification/base_read_count.txt
```
Comptage TE consensus
```sh
./src/check_step/base_TE_count.sh > res/verification/base_TE_count.txt
```

### Alignement TE/Read

Comptage long-read
```sh
./src/check_step/align_read_count.sh > res/verification/align_read_count.txt
```
Comptage TE consensus
```sh
./src/check_step/align_TE_count.sh > res/verification/align_TE_count.txt
```

### Filter

Comptage long-read FL/chimera
```sh
./src/check_step/filtered_FL_read_count.sh > res/verification/filtered_FL_read_count.txt
./src/check_step/filtered_chimera_read_count.sh > res/verification/filtered_chimera_read_count.txt
```
Comptage TE consensus FL/chimera
```sh
./src/check_step/filtered_FL_TE_count.sh > res/verification/filtered_FL_TE_count.txt
./src/check_step/filtered_chimera_TE_count.sh > res/verification/filtered_chimera_TE_count.txt
```

### Dectected

Parmis ceux qui sont bien détecter dans le génome

Comptage long-read FL/chimera
```sh
./src/check_step/detect_FL_read_count.sh > res/verification/detect_FL_read_count.txt
./src/check_step/detect_chimera_read_count.sh > res/verification/detect_chimera_read_count.txt
```
Comptage TE consensus FL/chimera
```sh
./src/check_step/detect_FL_TE_count.sh > res/verification/detect_FL_TE_count.txt
./src/check_step/detect_chimera_TE_count.sh > res/verification/detect_chimera_TE_count.txt
```

😀

