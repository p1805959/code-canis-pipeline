""" Clustering interval.
"""

import pandas as pd
import numpy as np
import argparse
from sklearn.cluster import DBSCAN 

# Shell interface

parser = argparse.ArgumentParser(
                    prog = 'intervalles.py',
                    description = 'From the precedent .tsv, create file describing new intervalles.',
                    epilog = 'For more information, contact pierre.gerenton@etu.univ-lyon1.fr.')

parser.add_argument("tsvpath",
                    help = "Path of a .tsv file.")
parser.add_argument("output_prefix", default=None,
                    help = "Prefix of the output file generated.")
parser.add_argument("-p","--dbscan_parameter",
                    help="DBSCAN's sklearn eps parameters",
                    required=False,
                    default=1000,
                    type=int)

args = parser.parse_args()

# lecture du fichier

data = pd.read_csv(args.tsvpath, sep="\t")

data = data[data["Chr"]!="*"]  # on enlève les reads qui ne se retrouve pas sur le génome

data = data.astype({"Start":int, "End":int})

liste_chr = np.unique(data["Chr"])

intervalles = pd.DataFrame(columns=["Chr","Start","End","TE Name","Group","Count"])

duree = len(liste_chr)
i = 0

for chromosome in liste_chr:
    i+=1
    if i%15==0:
        print(i, " / ",duree) 
    df = data[data["Chr"]==chromosome]

    #numpy-isation
    coord = df[["Start","End"]]
    coord = coord.values

    #clustering
    clf = DBSCAN(1000, min_samples=1)
    clf.fit(coord)

    df_te = pd.DataFrame()
    df_te["Group"] = np.unique(clf.labels_)
    df_te["Count"] = df_te["Group"].apply(lambda x: sum(clf.labels_==x))
    df_te["Start"] = df_te["Group"].apply(lambda x: min(df[clf.labels_==x]["Start"]))
    df_te["End"] = df_te["Group"].apply(lambda x: max(df[clf.labels_==x]["End"]))
    df_te["Chr"] = chromosome
    df_te["TE Name"] = "multi"

    intervalles = pd.concat([intervalles,df_te])

    
intervalles["Start"][intervalles["Start"]<0] = 0 # avoid bug
intervalles["End"][intervalles["End"]<0] = 0 # avoid bug


# Output file
# _intervalles_cluster.bed : bigger intervalles, bed format

print("File output :")

intervalles.to_csv(args.output_prefix + "_cluster_intervalles.bed", header=False, index=False, sep="\t")
print(args.output_prefix + "_cluster_intervalles.bed file usefull to represent intervalle feature")
