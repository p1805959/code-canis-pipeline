#!/bin/bash

set -e

echo "Merge testis"
head -n 1 res/annotation/aston_transposon_genome.tsv > res/annotation/testis_transposon_genome.tsv
tail -qn +2 res/annotation/aston_transposon_genome.tsv res/annotation/domino_transposon_genome.tsv res/annotation/emir_transposon_genome.tsv res/annotation/fute_transposon_genome.tsv res/annotation/samy_transposon_genome.tsv res/annotation/voyou_transposon_genome.tsv >> res/annotation/testis_transposon_genome.tsv
cat res/annotation/aston_transposon_genome.bed res/annotation/domino_transposon_genome.bed res/annotation/emir_transposon_genome.bed res/annotation/fute_transposon_genome.bed res/annotation/samy_transposon_genome.bed res/annotation/voyou_transposon_genome.bed > res/annotation/testis_transposon_genome.bed

echo "Merge cancer"
head -n 1 res/annotation/bear_transposon_genome.tsv > res/annotation/cancer_transposon_genome.tsv
tail -qn +2 res/annotation/bear_transposon_genome.tsv res/annotation/chipie_transposon_genome.tsv res/annotation/chrystalPoumon_transposon_genome.tsv res/annotation/chrystalRate_transposon_genome.tsv res/annotation/cml10_transposon_genome.tsv res/annotation/popsi_transposon_genome.tsv res/annotation/twiny_transposon_genome.tsv res/annotation/vico_transposon_genome.tsv >> res/annotation/cancer_transposon_genome.tsv
cat res/annotation/bear_transposon_genome.bed res/annotation/chipie_transposon_genome.bed res/annotation/chrystalPoumon_transposon_genome.bed res/annotation/chrystalRate_transposon_genome.bed res/annotation/cml10_transposon_genome.bed res/annotation/popsi_transposon_genome.bed res/annotation/twiny_transposon_genome.bed res/annotation/vico_transposon_genome.bed > res/annotation/cancer_transposon_genome.bed


echo "Testis :"

python3 ./src/intervalles_merge.py res/annotation/testis_transposon_genome.tsv ./res/annotation/testis_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/testis_transposon_genome.tsv ./res/annotation/testis_transposon_genome -p 1000

echo "aston :"
python3 ./src/intervalles_merge.py res/annotation/aston_transposon_genome.tsv ./res/annotation/aston_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/aston_transposon_genome.tsv ./res/annotation/aston_transposon_genome -p 1000
echo "domino :"
python3 ./src/intervalles_merge.py res/annotation/domino_transposon_genome.tsv ./res/annotation/domino_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/domino_transposon_genome.tsv ./res/annotation/domino_transposon_genome -p 1000
echo "emir :"
python3 ./src/intervalles_merge.py res/annotation/emir_transposon_genome.tsv ./res/annotation/emir_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/emir_transposon_genome.tsv ./res/annotation/emir_transposon_genome -p 1000
echo "fute :"
python3 ./src/intervalles_merge.py res/annotation/fute_transposon_genome.tsv ./res/annotation/fute_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/fute_transposon_genome.tsv ./res/annotation/fute_transposon_genome -p 1000
echo "samy :"
python3 ./src/intervalles_merge.py res/annotation/samy_transposon_genome.tsv ./res/annotation/samy_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/samy_transposon_genome.tsv ./res/annotation/samy_transposon_genome -p 1000
echo "voyou :"
python3 ./src/intervalles_merge.py res/annotation/voyou_transposon_genome.tsv ./res/annotation/voyou_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/voyou_transposon_genome.tsv ./res/annotation/voyou_transposon_genome -p 1000


echo "Cancer :"

python3 ./src/intervalles_merge.py res/annotation/cancer_transposon_genome.tsv ./res/annotation/cancer_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/cancer_transposon_genome.tsv ./res/annotation/cancer_transposon_genome -p 1000


echo "bear :"
python3 ./src/intervalles_merge.py res/annotation/bear_transposon_genome.tsv ./res/annotation/bear_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/bear_transposon_genome.tsv ./res/annotation/bear_transposon_genome -p 1000
echo "chipie :"
python3 ./src/intervalles_merge.py res/annotation/chipie_transposon_genome.tsv ./res/annotation/chipie_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chipie_transposon_genome.tsv ./res/annotation/chipie_transposon_genome -p 1000
echo "chrystalPoumon :"
python3 ./src/intervalles_merge.py res/annotation/chrystalPoumon_transposon_genome.tsv ./res/annotation/chrystalPoumon_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chrystalPoumon_transposon_genome.tsv ./res/annotation/chrystalPoumon_transposon_genome -p 1000
echo "chrystalRate :"
python3 ./src/intervalles_merge.py res/annotation/chrystalRate_transposon_genome.tsv ./res/annotation/chrystalRate_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/chrystalRate_transposon_genome.tsv ./res/annotation/chrystalRate_transposon_genome -p 1000
echo "cml10 :"
python3 ./src/intervalles_merge.py res/annotation/cml10_transposon_genome.tsv ./res/annotation/cml10_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/cml10_transposon_genome.tsv ./res/annotation/cml10_transposon_genome -p 1000
echo "popsi :"
python3 ./src/intervalles_merge.py res/annotation/popsi_transposon_genome.tsv ./res/annotation/popsi_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/popsi_transposon_genome.tsv ./res/annotation/popsi_transposon_genome -p 1000
echo "twiny :"
python3 ./src/intervalles_merge.py res/annotation/twiny_transposon_genome.tsv ./res/annotation/twiny_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/twiny_transposon_genome.tsv ./res/annotation/twiny_transposon_genome -p 1000
echo "vico :"
python3 ./src/intervalles_merge.py res/annotation/vico_transposon_genome.tsv ./res/annotation/vico_transposon_genome -i
python3 ./src/intervalles_cluster.py res/annotation/vico_transposon_genome.tsv ./res/annotation/vico_transposon_genome -p 1000
