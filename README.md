# Projet d'analyse de donnée du Chien *Canis lupus familiaris* et de détection d'élément transposable

## Prise en main

Tous les scripts sont a executer depuis la racine du projet.

```sh
./src/fetch_data.sh
```

Permet de télécharger les données du projet. La description de ces données est disponible dans doc/data_desc.txt .
Télécharge les données fastq long_read, mais aussi leurs alignements sur le génome de référence canfam4 et l'index de ces alignments au format .bam et .bai respectivement.

## Architecture du projet

- src : dossier contenant les scripts
- doc : dossier contenant la documentation
- res : contient les données et les résultats des scripts
    - res/data : contient les données d'entrée
    
On trouve environ 52 789 816 reads dans la condition cancer, et 9 897 957 reads dans la conditions testicle.

<!-- zcat cancer.fastq.gz | wc -l
zcat testicle.fastq.gz | wc -l -->
<!-- 1821660 pour Aston
4002891 pour Bear


748 TE dans RU pour Clupusf

 -->

## Éxécution de script

### Trouver des reads avec des TEs

```sh
./src/find_read_with_TE.sh
```

Ce premier script va chercher les reads avec des éléments transposables du consensus de Canis lupus familiaris.
Deux fichier vont être produit dans res/alignment pour chaque condition (cancer et testicle) :

- [cancer/testicle]_reads2TE.sam : résultat de l'alignement des reads sur les TE
- [cancer/testicle]_reads_with_TE.sam : fichier avec les lignes correspondantes à aucun alignement enlevé

On trouve respectivement 3637199 et 62233 reads avec des TEs pour chaque condition.

<!-- 
Compte ligne :
samtools view aston_reads_with_TE.sam | wc -l
25252 pour Aston
323192 pour Bear 

Compte read unique :
samtools view aston_reads_with_TE.sam | awk '{print $1}' | sort | uniq | wc -l
Aston : 11616
Bear : 128368

TE unique chez Aston : 108
TE unique chez Bear : 166
-->

### Garder des reads intéressants

```sh
./src/keep_FL_read.sh
```

Ce script va filtrer les reads en appelant `filter_read.py` pour garder ceux qui ont des éléments transposables plutôt complet (ref coverage >= 80%) et avec de l'ARN ne provenant pas de transposons (query coverage <= 50%).

Deux fichier par conditions sont également produits :

- res/alignment/[cancer/testicle]_filtered_read.sam : le fichier reads_with_TE.sam filtré selon les conditions ci-dessus.
- res/sequence/[cancer_testciel]_read_with_masked_TE.fa : les reads avec l'élément masqué 
(ne marche pas actuellement)

On a respectivement 1468237 et 26749 reads filtrés.

<!-- 
Compte ligne :
samtools view aston_filtered_read.sam | wc -l
15215 pour Aston
235512 pour Bear 

Compte read unique :
samtools view aston_filtered_read.sam | awk '{print $1}' | sort | uniq | wc -l
Aston : 7387
Bear : 94527

TE unique chez Aston : 51
TE unique chez Bear : 106
-->

### Détecter les TE 

```sh
./src/detectTE.sh
```

Ce script va trouver la position des TE trouvés dans les reads mais dans le génome. Deux fichiers de sorties seront données :
- res/annotation/[cancer/testicle]_transposon_genome.bed : fichier .bed avec le nom, l'indicateur et la position de chaque transposons trouvés dans le génome
- res/annotation/[cancer/testicle]_transposon_genome.tab : le même que précédemment, moins pratique à utiliser dans les outils car pas de format bed, mais avec l'id du read d'où provient l'élément


### Création d'intervalle de transposons avec compte

Étant donné que chace TE de chaque Read donne un intervalle, il est vite long de chercher ceux qui sont les plus exprimés. Une approche efficace pour le faire est de fusionner entre eux les intervalles chevauchants pour pouvoir comparer les reads du génome dessus pour avoir leurs expressions. On peut aussi regarder les grands intervalles qui sont composer le plus de petit intervalles : ces derniers garentirait que plusieurs reads ont trouvé un transposon à cette endroit.

```sh
./src/intervalles.sh
```

<!--
cat res/annotation/testis_transposon_genome_merge_equivalence.tsv | sort -t "	" -nk 7 | awk -F '	' '{print $1"	"$2"	"$3"	"$5"	"$7}'
cat res/annotation/testis_transposon_genome_cluster_intervalles.bed | sort -t "	" -nk 6 | awk -F '	' '{print $1"	"$2"	"$3"	"$6}'
-->


### Analyse des chimères

Les chimères sont des reads avec des TE mais pas que. On doit donc reprendre au niveau du filtre.

```sh
./src/keep_chimera_read.sh
./src/detectChimera.sh
./src/intervalles_chimera.sh
```

### Tri des intervalles

```sh
./src/sort_intervalles.sh
```