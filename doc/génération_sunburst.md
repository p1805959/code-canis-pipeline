<!> Toutes les commandes sont lancés à la racine du projet !
La majorité du travail est fait avec plotly sur Python, mais certains actions sont fait au préalable sur le shell pour simplifier la tâche.

## Histogramme de RU

Génération d'une liste des noms de séquences de RU

```sh
mkdir -p res/sunburst
cat res/data/ref/transposons_consensus.fa | grep ">" > res/sunburst/name_transposons.tsv
```

Le fichier `name_transposons.tsv` est de la forme :
\>nomseq\<tab\>famile\<tab\>clade

L'histogramme montrant la richesse de chaque famille de base est produit. Cela ne représente pas de compte !
C'est fait avec `src/make_sunburst.ipynb`.


## Sunburst avant filtre

Comme pour le travail précédent, on prétraite en shell.
Ici on va isoler le nom des TE dans notre jeu de données et compter l'occurence de chacun.

 ```sh
samtools view res/alignment/aston_reads_with_TE.sam | awk '{print $3}' | sort | uniq -c | awk '{print $NF"\t"$(NF-1)}' > res/sunburst/aston_prefilter_count.tsv
samtools view res/alignment/bear_reads_with_TE.sam | awk '{print $3}' | sort | uniq -c | awk '{print $NF"\t"$(NF-1)}' > res/sunburst/bear_prefilter_count.tsv
```

Puis on génère des sunbursts avec `src/make_sunburst.ipynb`.

D'autre histogramme plus détaillé sont aussi produit sur le notebook.
La diversté des TE avant et après le filtre peuvent etre calculé.

## Sunburst après filtre

Même procédé, mais avec ces commandes de prétraitement :

 ```sh
samtools view res/alignment/aston_filtered_read.sam | awk '{print $3}' | sort | uniq -c | awk '{print $NF"\t"$(NF-1)}' > res/sunburst/aston_postfilter_count.tsv
samtools view res/alignment/bear_filtered_read.sam | awk '{print $3}' | sort | uniq -c | awk '{print $NF"\t"$(NF-1)}' > res/sunburst/bear_postfilter_count.tsv
```


Les images des sunbursts sont ensuite rogner avec Shotwell.